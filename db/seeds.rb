require 'csv'

csv_data = CSV.read("db/data/books.csv")
csv_data.each do |row|
  Book.create(title: row[0], price: row[1], author_names: row[2], description: row[3])
end