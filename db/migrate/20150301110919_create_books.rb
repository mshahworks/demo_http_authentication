class CreateBooks < ActiveRecord::Migration
  def change
    create_table :books do |t|
      t.string :title
      t.text :description
      t.boolean :published, default: true
      t.float :price, default: 0.0
      t.text :author_names
      t.timestamps null: false
    end
  end
end