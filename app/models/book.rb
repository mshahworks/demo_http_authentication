class Book < ActiveRecord::Base

  ## Validations ##
  validates :title, :price, :author_names, :description, presence: true

  ## Scope ##
  scope :published_books, -> { where(published: true) }
end